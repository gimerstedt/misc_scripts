const cp = require("child_process");
const Promise = require("bluebird");

function exec(cmd) {
  return new Promise((yay, nay) => {
    cp.exec(cmd, (e, stdout, stderr) => {
      if (e) nay(stderr);
      yay(stdout);
    });
  });
}

module.exports = exec;
