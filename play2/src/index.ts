import {exec} from 'child_process';
import * as fs from 'fs';

// config
const player = 'mpv';
const additionalPlayerArgs = '-fs';
const media = /\.(rar|mp4|mkv|avi|wmv)$/;
const ignore = /(Sample|sample)/;

const error = (msg: any) => {
  console.log(msg);
  process.exit(1);
};
const info = (msg: any) => console.log(msg);

const nohup = (cmd: string) => exec('nohup ' + cmd + ' > /dev/null 2>&1 &');
const isFile = (arg: string) => fs.statSync(arg).isFile();
const notIgnored = (f: string) => !f.match(ignore);
const playable = (f: string) => !!f.match(media);
const cd = (dir: string) => {
  try {
    process.chdir(dir);
    return true;
  } catch {
    return false;
  }
};

const findFile = (dir: string) =>
  fs
    .readdirSync(dir)
    .filter(isFile)
    .filter(notIgnored)
    .find(playable);

// play
const play = (arg = '.') => {
  const file = isFile(arg) ? arg : cd(arg) && findFile('.');
  file
    ? info(`playing "${file}" with ${player}`)
    : error(`no media found in ${arg}`);
  nohup(`${player} ${additionalPlayerArgs} "${file}"`);
};

play(process.argv[2]);
