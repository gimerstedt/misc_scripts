#!/bin/sh
path=/home/krig

bash $path/git/misc_scripts/status_wallpaper/generate.sh > $path/.local/share/status.svg
convert -background "#333" $path/.local/share/status.svg $path/.local/share/status.png
DISPLAY=:0 feh --bg-fill $path/.local/share/status.png
