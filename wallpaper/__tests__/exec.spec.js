const sut = require("../src/exec");

describe("exec", () => {
  it("should ps...", () => {
    sut("ps")
      .then(stdout => {
        expect(stdout.split("\n")[0]).toContain("CMD");
      })
      .catch(e => {
        expect(e).toBeNull();
      });
  });

  it("should fail...", () => {
    sut("asdfasdfasdfasdf")
      .then(stdout => {
        expect(stdout).toBeNull();
      })
      .catch(e => {
        expect(e).toContain("not found");
      });
  });
});
