const el = element => document.createElement(element);

const data = [
  { name: "yc", href: "https://news.ycombinator.com/" },
  { name: "news", href: "https://news.google.com/" },
  { name: "phoronix", href: "https://phoronix.com/" },
  { name: "/r/linux", href: "https://reddit.com/r/linux" },
  {
    name: "tltv",
    href: "https://goo.gl/YkEmwi"
  },
  {
    name: "tl24",
    href: "https://goo.gl/B8BpsJ"
  },
  { name: "fb", href: "https://fb.com/" },
  { name: "alarm", href: "http://military.onlineclock.net/" }
];

class List {
  constructor(data) {
    this.data = data;
  }
  build() {
    const ret = el("ul");
    for (let i of this.data) {
      const li = el("li");
      const a = el("a");

      a.innerHTML = i.name;
      if (i.href) {
        a.href = i.href;
      } else if (i.onclick !== undefined) {
        a.onclick = i.onclick;
      }

      ret.appendChild(li.appendChild(a));
    }
    return ret;
  }
}

const updateKlocka = () => {
  const now = new Date();
  const tid = `${ now.getHours() }:${ now.getMinutes() }:${ now.getSeconds() }`;
  document.body.getElementsByTagName('klocka')[0].innerHTML = tid;
}

const init = () => {
  document.body.appendChild(new List(data).build());
  document.body.appendChild(el('klocka'));
  updateKlocka();
  setInterval(updateKlocka, 1000);
}

window.onload = () => init();
