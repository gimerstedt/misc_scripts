#!/bin/bash
height=1440
width=2560
text="a wallpaper"
fg_color="#ddd"
bg_color="#333"
nok_color="#f66"
ok_color="#6f6"
font="Hasklug NF"
font_size=256
font_size_status=28
url="https://sjobasis-veriff.kbv.se/sysstat/status"
json=$(curl -sk "$url")

get_status() {
	status=$(echo "$json" | jq ".\"$1\".response.status" | cut -d '"' -f 2 | tr '[:upper:]' '[:lower:]')
	echo "$1: $status"
}

status_svg() {
	if [[ "$1" != *"up"* ]]
	then
		l_color=$nok_color
	else
		l_color=$ok_color
	fi
	name=$(echo "$1" | cut -d ':' -f 1)
	x=$(echo ${width}-420 | bc)
	y=$(echo ${height}-"$2" | bc)
	svg=$(cat <<EOF
<text
x="$x"
y="$y"
font-family="$font"
font-size="$font_size_status"
fill="$l_color">$name</text>
EOF
)
	echo "$svg"
}

status() {
	status_svg "$(get_status ${1})" "${2}"
}

anvandare=$(status anvandare 400)
sjoinfo=$(status sjoinfo 360)
sjolage=$(status sjolage 320)
sjolage_ais=$(status "sjolage-ais" 280)
sjolage_rakel=$(status "sjolage-rakel" 240)
sjolage_vms=$(status "sjolage-vms" 200)
sjolage_sjoc=$(status "sjolage-sjoc" 160)
varningar=$(status varningar 120)
inloggning_federation=$(status "inloggning-federation" 80)

cat <<EOF
<svg xmlns="http://www.w3.org/2000/svg" width="$width" height="$height" viewBox="0 0 $width $height" style="background:$bg_color">
	<meta charset="utf-8"/>
	<text
		x="$(echo $width/2 | bc)"
		y="$(echo $height/2 | bc)"
		font-family="$font"
		font-size="$font_size"
		fill="$fg_color"
		text-anchor="middle"
		dominant-baseline="middle">$text</text>
		<rect y="$(echo ${height}-450 | bc)" x="$(echo ${width}-450 | bc)" width="400" height="400" style="stroke-width:3;stroke:$fg_color;fill:$bg_color"/>
		$anvandare
		$sjoinfo
		$sjolage
		$sjolage_ais
		$sjolage_rakel
		$sjolage_vms
		$sjolage_sjoc
		$varningar
		$inloggning_federation
</svg>
EOF
