#!/bin/sh
trap cleanup EXIT
tempfile=$(mktemp)
arg=${*}

cleanup() {
	rm "$tempfile"
}

set_font_size() {
	fontsize=${*}

	sed "s/^\(urxvt\.font:.*:size=\)[0-9]*/\1${fontsize}/g" ~/.Xresources > "${tempfile}"
	xrdb "$tempfile"
}

case $arg in
	''|*[!0-9]*)
		echo "That's not a fontsize..."
		;;
	*)
		set_font_size "$arg"
		;;
esac
